# book example
# book = {
#    'name': 'Programming with Python 3',
#    'pages': 157,
#    'read': True
# }

"""
Nejdriv je nutne si uvedomit jakou vlastne strukturu bude mit knihovna (v reprezentaci Pythonu)
Knihovna bude reprezentovana seznamem a kniha bude reprezentovana slovnikem.
Prazdna knihovna je videt o dva radky niz. Pokud by v knihovne byla nejaka knizka, tak bude knihovna vypadat nejak takhle:
[False, False, {'name': 'Programming with Python 3', 'pages': 157, 'read': True}, False, False, False, False, False, False, False]
"""
bookshelf = [False] * 10  # = [False, False, False, False, False, False, False, False, False, False]


def is_free(position):
    """
    Funkce zkontroluje jestli je na zadane pozici volno (tzn. jestli je hodnota False)
    :param (int) position: pozice v seznamu, ve kterem se bude zjistovat obsazenost
    :return (bool): vrati True, kdyz je pozice volna a False, kdyz pozice v knihovne volna neni
    """
    # do seznamu (nekdy muzu pouzit i slovo list, pole, array apod.) se pristupuje pomoci indexu (prvni pozice 0, dalsi 1 atd.)
    # nam tedy staci vybrat hodnotu na dane pozici bookshelf[position] a tu znegovat pomoci not, protoze pokud bude pozice prazdna
    # (bude se rovnat False, tak negaci dostaneme a vratime True). Pokud se na pozici bude nachazet slovnik ktery nebude prazdny,
    # tak ziskame hodnotu True a pomoci not ziskane False
    # muzete si to vyzkouset tak, ze zadate do konzole tohle:
    # >>> not {}
    # True
    # >>> not {'name': 'python'}
    # False
    # Tohle berte trochu s rezervou (zadani tady neni prilis dobre, spolehat na to, ze slovnik nebude prazdny je dost nesikovne...)
    return not bookshelf[position]


def add_book(position, name, pages, read=False):
    """
    Funkce prida knihu do seznamu na zadanou pozici
    :param (int) position: pozice v seznamu, na kterou novou knihu pridate
    :param (str) name: nazev knihy
    :param (int) pages: pocet stran nove knihy
    :param (bool) read: informace jestli je kniha prectena nebo ne
    :return: None
    """
    # muzeme pouzit predchozi funkci, ktera nam zjistuje to, jestli je na miste volno nebo ne
    # is_free vraci True nebo False. Pokud vrati True, tak je podminka splnena a mi pridame na pozici (promenna position) slovnik,
    # ktery predstavuje knihu
    if is_free(position):
        bookshelf[position] = {'name': name,
                               'pages': pages,
                               'read': read}


def find_book(name):
    """
    Funkce, ktera vyhleda knihu podle nazvu a vrati index, na kterem se hledana kniha nachazi
    :param (str) name: nazev knihy
    :return (int): index na kterem se kniha nachazi, popripade False pokud se tam kniha nenachazi
    """
    # pro prohledani slovniku a vyhledani knihy podle nazvu je nejjednodussi projit celym seznamem, takze potrebujeme cyklus
    # prochazime promennou bookshelf, ktera v sobe obsahuje knihy nebo prazdna mista a kazdou hodnotu si davame do promenne book
    # na radku 70 zjistujeme jestli je kniha typu slovnik. Mohlo by se totiz stat, ze pozice bude prazdna (tudiz bude hodnota False)
    # a tim padem bychom dale vybirali name z False, coz je hloupost a zahlasilo by nam to chybu
    # funkci isinstance(o,t) si muzete projit tady: https://www.w3schools.com/python/ref_func_isinstance.asp
    # prvni parametr je hodnota a druhy je typ - vraci se True pokud je promenna typu, ktery ja na druhem parametru
    # v dalsi podmince vybereme ze slovniku pomoci klice name nazev knihy a srovname ho s nazvem, ktery obdrzela funkce
    # pokud se nazvy rovnaji tak pomoci funkce index (https://www.tutorialspoint.com/python/list_index.htm)
    #  vratime index, na kterem se kniha nachazi. Jinak vratime False
    for book in bookshelf:
        if isinstance(book, dict):
            if book['name'] == name:
                return bookshelf.index(book)
    return False


def read_book(name):
    """
    Funkce, ktera nastavi knize hodnotu read na True pokud kniha v knihovne existuje
    :param (str) name: nazev knihy
    :return: None
    """
    # tady pouzijeme funkci find_book().
    # JE TADY ALE CHYBA!!!
    # pokud totiz budeme hledat knihu, ktera bude na indexu 0, tak funkce find_book() vrati index 0
    # 0 == False, tim padem nam podminka neprojde a funkce vrati None
    if find_book(name):
        bookshelf[find_book(name)]['read'] = True


def remove_read_books():
    """
    Funkce, ktera nahradi vsechny prectene knihy (knihy s hodnotou read = True) na False (prazdne misto knihovny)
    :return: None
    """
    # tohle je stejne pripad jako find_book(), akorat s tim rozdilem, ze na indexu nastavime False
    # zase pouzijeme isinstance(o,t), protoze chceme vymazat pouze misto, kde puvodne byl slovnik
    for book in bookshelf:
        if isinstance(book, dict):
            if book['read']:
                bookshelf[bookshelf.index(book)] = False


def names_of_books():
    """
    Funkce vraci seznam se jmeny vsech knih v knihovne
    :return (list): seznam se jmeny vsechn knih v knihovne
    """
    # tady chceme vratit jenom nazvy knih. Takze musime projit sezname (bookshelf) a vybrat slovniky
    # z tech pak vybereme nazvy knih
    # vytvorime si novou promennou s prazdnym seznamem - tady pak budeme davat nazvy knih
    # iterujeme pole a hledame jenom slovniky (knihy). Pokud je to slovnik tak vybereme nazev knihy a pomoci
    # metody list.append(o) ho pridame do seznamu names. Ten pak vratime
    names = []
    for book in bookshelf:
        if isinstance(book, dict):
            names.append(book['name'])
    return names


def average_number_of_pages():
    """
    Funkce vraci prumerny pocet stran knih v knihovne
    :return (int): prumerny pocet stran v knihovne
    """
    # tady si musime nejdriv vytvorit dve promenne sum - kde budeme ukladat celkovy pocet stran a counter - kde
    # si budeme davat pocet knih. Opet budeme iterovat knihovnou a hledat jenom slovniky (knihy).
    # Pokud najdeme knihu tak pricteme do promenne sum pocet stranek dane knihy a pripocteme 1 ke counteru
    # nakonec spolu hodnoty podelime a muzeme ho prevest jeste na datovy typ int, aby nam to vracelo 183 misto 183.0
    sum = 0
    counter = 0
    for book in bookshelf:
        if isinstance(book, dict):
            sum += book['pages']
            counter += 1
    return int(sum/counter)


# testing

# 1
add_book(0, 'A Byte of python', 120, read = True)
add_book(1, 'Automate the boring stuff with Python', 420)
add_book(2, 'Python for Informatics', 500, read = True)
add_book(3, 'Learning Geospatial Analysis with Python', 155)
add_book(4, 'Python Geospatial Development', 211)
add_book(5, 'Python for Data Analysis', 362, read = True)
add_book(5, 'Programming Java', 874)

print(is_free(5)) # False
print(is_free(6)) # True


# 2
print(find_book('A Byte of python')) # 0
print(find_book('Python Geospatial Development')) # 4
print(find_book('Programming Java')) # False

# 3
read_book('A Byte of python')
read_book('Automate the boring stuff with Python')
read_book('Programming Java')


# 4
remove_read_books()
print(is_free(1)) # True
print(is_free(3)) # False


# 5
print(names_of_books()) # ['Learning Geospatial Analysis with Python', 'Python Geospatial Development']


# BONUS
print(average_number_of_pages()) # 183


# HINT
print(bookshelf)
# [False, False, False, {'pages': 155, 'name': 'Learning Geospatial Analysis with Python', 'read': False}, {'pages': 211, 'name': 'Python Geospatial Development', 'read': False}, False, False, False, False, False]


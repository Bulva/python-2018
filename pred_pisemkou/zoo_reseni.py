import ast

# Byli jste pozadani mistni ZOO, abyste pro ne vytvorili primitivní informacni system, ktery bude obsahovat
# informace o zvířatech v ZOO.

# System bude umět přidat a odstranit zvire ze systemu,
# bude mozne zadat krmeni zvirete, zkontrolovat jeho zdravotni stav
# Informace v systemu jsou predstaveny ve forme pole, ktere obsahuje jednotliva zvirata (slovníky), viz promenna animals
# Kdo chce, muze pouzit rovnou funkce, ale neni to nutnost. Ukazeme si rozdil na pristi hodine

animals = [
    {'species': 'wolf', 'name': 'Boris', 'sex': 'male', 'fed': True, 'healthy': True},
    {'species': 'wolf', 'name': 'Sable', 'sex': 'female', 'fed': False, 'healthy': True},
    {'species': 'elephant', 'name': 'Suki', 'sex': 'female', 'fed': True, 'healthy': False},
    {'species': 'eagle', 'name': 'Comet', 'sex': 'male', 'fed': False, 'healthy': False},
    {'species': 'panda', 'name': 'Zelda', 'sex': 'female', 'fed': True, 'healthy': True},
]

# 2: nakrmeni zvirat: prochazejte polem a pokud nektere ze zvirat bude mit fed == False, tak vypiste do konzole,
# ze dane zvire bylo nakrmeno a nastavte hodnotu fed = True
def feed_animal():
    for animal in animals:
        if not animal['fed']:
            animal['fed'] = True
            print('Zvíře {} se jménem {} bylo nakrmeno.'.format(animal['species'], animal['name']))


# 3: vyleceni zvirat: obdobne jako u 2
def heal_animal():
    for animal in animals:
        if not animal['healthy']:
            animal['healthy'] = True
            print('Zvíře {} se jménem {} bylo vyléčeno.'.format(animal['species'], animal['name']))


# 4: nalezeni zvirat co maji hlad: prochazejte pole a pro kazde zvire zkontrolujte zda nema hlad. Pokud bude mit hlad,
# tak do konzole vypiste informace o zviratech co maji hlad vcetne celkoveho poctu zvirat co maji hlad (napr. Celkovy pocet hladovych zvirat: 2)
def find_hungry():
    count = 0
    for animal in animals:
        if not animal['fed']:
            print('Hladovějící zvíře {} se jménem {}.'.format(animal['species'], animal['name']))
            count += 1
    print('Celkový počet hladovějících zvířat: {}'.format(count))


# 5: obdobne jako u 4, akorat pro klíč healthy
def find_ill():
    count = 0
    for animal in animals:
        if not animal['healthy']:
            print('Němocné zvíře {} se jménem {}.'.format(animal['species'], animal['name']))
            count += 1
    print('Celkový počet nemocných zvířat: {}'.format(count))


# 6: Vypište nějak pěkne zformátovaný seznam všech zvířat v ZOO
# nevypisujte jen slovníky ve formě {'species': 'eagle'...}, ale vypiste zvirata v nejake kultivovanejsi forme
def print_animals():
    for animal in animals:
        print('Zvíře: {}, jméno: {}, pohlaví: {}, nakrmen: {}, zdravi: {}'.format(
            animal['species'], animal['name'], animal['sex'], animal['fed'], animal['healthy']
        ))


NAME_OF_THE_ZOO = ''
print('Vítejte v informačním systému ZOO {}'.format(NAME_OF_THE_ZOO))

while True:
    answer = int(input('Vyberte jednu z možností:\n'
                       '1: Přidat nové zvíře\n'
                       '2: Nakrmit zvířata\n'
                       '3: Výlečit zvířata\n'
                       '4: Najít zvířata co mají hlad\n'     # fed == False
                       '5: Nají zvířata co jsou nemocná\n'   # healthy == False
                       '6: Vypsat všechna zvířata\n'
                       '7: Ukončit systém\n'))

    if answer == 1:
        specie = input('Zadejte druh: \n')
        name = input('Zadejte jméno: \n')
        sex = input('Zadejte pohlaví: \n')
        fed = ast.literal_eval(input('Zadejte zda je zvíře nakrmeno: \n')  )   # zadavejte True nebo False
        healthy = ast.literal_eval(input('Zadejte zda je zvíře zdravé: \n'))   # zadavejte True nebo False
        print(healthy)
        animals.append({'species': specie, 'name': name, 'sex': sex, 'fed': fed, 'healthy': healthy})
    elif answer == 2:
        feed_animal()
    elif answer == 3:
        heal_animal()
    elif answer == 4:
        find_hungry()
    elif answer == 5:
        find_ill()
    elif answer == 6:
        print_animals()
    elif answer == 7:
        print('Ukončování programu...')
        break

'''
Navrhnete strukturu a funkcionalitu pro system udrzujici jizdni rady
Pole (list) bude predstavovat (databazi jizdnich radu) a jizdni rady budou představeny
slovnikem, ktery bude obsahovat nasledujici vlastnosti:
  ID trasy: integer, např. 1. Každé trase vyberte identické číslo
  misto odjezdu: nazev mesta
  misto prijezdu: nazev jineho mesta
  cas odjezdu: ve formatu HH:mm (napr. 18:00) typu retezec ('18:00'). Kdo chce muze vyuzit datovy typ datetime (https://docs.python.org/3/library/datetime.html)
  cas prijezdu: obdobne jako cas odjezdu
  typ dopravního prostředku: řetězec
  vzdalenost: vzdalenost mezi mesty v km
  kilometrovou sazbu: cislo typu float (napr. 1.50)

Celý slovník představující linku by mohl vypadat nějak takto:
{'id': 1, 'odjezd_mesto': 'Brno', 'prijezd_mesto': 'Praha', 'odjezd_cas': '7:00', 'prijezd_cas': '9:30', 'prostredek': 'autobus', 'vzdalenost': 200, 'sazba': 1.50}

Systém bude umět (znamená definujte funkce, které budou dělat) následující:
1. Zadat do systému jednu novou trasu - pokud uzivatel zada trasu se stejným ID, vypište hlášku, že zadaná trasa již existuje trasu do pole nepřidejte
2. Zadat do systému více nových tras najednou
3. Vypsat vsechny informace o trase, podle ID
4. Vypište trasy, které jsou obsluhovány dopravním prostředkem, který uživatel zadá
5. Vypočítejte cenu cesty, kterou uzivatel bude chtit spocitat (podle ID). Použijte vzdalenost a kilometrovou sazbu
6. Vymazani trasy podle zadaneho ID. Uzivatel napriklad zada ze chce vymazat trasu s ID 5 a vy danou trasu vymazete
7. Vypocitejte kolik minut dana linka jede. Najdete funkci, ktera vam cas rozdeli podle dvojtecky cimz ziskate hodiny a minuty zvlast. Z tech se pokuste vypocitat cas cesty v minutach

Na úvod si v kódu systém naplňte několika trasama ať je nemusíte vždycky vyplňovat ručně
Celému návrhu ponechávám volnou ruku. Je na vás jak navrhnete a naprogramujete systém, ale pokuste se ho nejak naprogramovat
Správnost řešení pak můžeme probrat osobně

PORADNE SI PRECTETE ZADANI A PROMYSLETE HO


Pokud si s něčím nebudete ani po týdenním zoufalství vůbec vědět rady, tak udělejte aspoň to co znáte a na zbytek se klidně zeptejte přes mail nebo osobně na příští hodině.
'''